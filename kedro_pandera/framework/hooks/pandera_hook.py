import logging
from kedro.framework.context import KedroContext
from pandera.errors import SchemaError
from kedro.framework.hooks import hook_impl
from kedro.framework.hooks.specs import hook_spec
from kedro.io import DataCatalog
from typing import Any
from omegaconf.resolvers import oc

from kedro_pandera.framework.config.resolvers import (
    resolve_dataframe_model,
    resolve_interpolated_yaml_schema,
    resolve_yaml_schema,
)

# if we do not import ``frictionless`` manually here, we get
# >  ImportError: ('# ERROR: failed to register fsspec file#systems', TypeError("argument of type '_Cached' is not iterable"))
# if we try to use ``Schema.to_yaml()`` after ``context.catalog``
# The command ``kedro pandera infer-schema -d example_iris_data``
# then raises the very useless error:
# > ImportError: IO and formatting requires 'pyyaml', 'black' and 'frictionless'to be installed.
# > You can install pandera together with the IO dependencies with:
# > pip install pandera[io]
# despite all the dependencies being properly installed

class PanderaHook:
    @property
    def _logger(self) -> logging.Logger:
        return logging.getLogger(__name__)
    
    @hook_impl
    def after_context_created(
        self,
        context: KedroContext,
    ) -> None:
        """Hooks to be invoked after a `KedroContext` is created. This is the earliest
        hook triggered within a Kedro run. The `KedroContext` stores useful information
        such as `credentials`, `config_loader` and `env`.
        Args:
            context: The context that was created.
        """
        context.config_loader._register_new_resolvers(
            {
                "pa.dict": resolve_yaml_schema,
                "pa.yaml": resolve_interpolated_yaml_schema,
                "pa.python": resolve_dataframe_model,
            }
        )
    
    @hook_spec
    def before_input_save(
        self,
        catalog: DataCatalog, 
        dataset_name:str, 
        data: Any
    ) -> None:
        pass
    
    @staticmethod
    def check_data(logger, catalog, dataset_name, data, node_name=''):
        if (
            catalog.__dict__['_datasets'][dataset_name].metadata is not None
            and "pandera" in catalog.__dict__['_datasets'][dataset_name].metadata
        ):
            try:
                catalog.__dict__['_datasets'][dataset_name].metadata["pandera"]["schema"].validate(data)
            except SchemaError as err:
                logger.error(
                    f"Dataset '{dataset_name}' pandera validation failed '{node_name}', see details in the error message. "
                )
                raise err
            except Exception as err:
                logger.error(
                    f"Dataset '{dataset_name}' validation raised an unknown error '{node_name}'"
                )
                raise err
            logger.info(
                f"(kedro-pandera) Dataset '{dataset_name}' was successfully validated with pandera"
            )
                    
    @hook_impl
    def before_node_run(
        self, node, catalog, inputs, is_async, session_id
    ):
        for dataset_name, data in inputs.items():
            self.check_data(self._logger(), catalog, dataset_name, data, node.name)
    
    @hook_impl
    def before_input_save(self, catalog, dataset_name, data):
        self.check_data(self._logger(), catalog, dataset_name, data)


pandera_hook = PanderaHook()